# DNA Translator #


### Purpose ###

The purpose of this application is to offer an easy tool to determine the possible translations of a dnaSequence. To achieve this the user can input a dnaSequence in FASTA format, select various options for processing and then submit this data to get the result. The user can select which frames of translation need to be displayed and the start and stop location. This tool also marks the possible open reading framees (ORF's) for which the user can also enter a minimum lenngth.

### Installation ###

This application is easy to install and set-up. In order to use this application you will need a distribution of 'Apache tomcat' (http://tomcat.apache.org/).

When tomcat is set up you can clone or download the repository, after this you need to run the gradle task 'war'. This will create a WAR file which needs to be put in the Apache Tomcat 'webapps' folder. When this is done the webapp should be accesible at the location of the tomcat process (by default 'localhost:8080').

#### Usage examples ####

![Image 1: The input screen - Part 1](/useCaseImages/input.PNG)

In image 1 and 2 the input screen is show, the user can enter a dnaSequence in FASTA format here,
enter the minimum length of the ORF and the start and stop location. NOTE: the end location is calculated from
the END of the dnaSequence (e.g. if the dnaSequence is 120 characters long and a stop value of 10 is used the first 110 characters
will be used in the translation). The user can also select the desired frames for the translation, if 6-frame
is selected all possibilities will be calculated and shown, because of this no other options can be selected.

![Image 2: The input screen - Part 2](/useCaseImages/input2.PNG)

In image 3 the result screen is shown, the blue lines indicate ORF's on the forward strand and the green
lines indicate ORF's on the reverse strand. The order of the frames is +1, +2, +3, -3, -2, -1 with the dnaSequence and complement
strand between the forward and reverse strand. On this screen the history is also shown, allowing users to
easily open earlier results.

![Image 3: The result screen](/useCaseImages/resultscreen.PNG)


### Who do I talk to? ###

Sven Hakvoort  
Owner and Developer at Hakvoort Development    
sven@hakvoortdevelopment.nl