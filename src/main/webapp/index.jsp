<%--
  Created by IntelliJ IDEA.
  User: Sven
  Date: 22/11/2017
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/includes/header.jsp"/>

<c:if test="${requestScope.failed != null}">
    <div class="alert alert-danger" role="alert">
            ${requestScope.failed}
    </div>
</c:if>
<c:if test="${requestScope.success != null}">
    <div class="alert alert-success" role="alert">
            ${requestScope.success}
    </div>
</c:if>

<%-- Input selector options --%>
<div class="container mt-4">
    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-success active">
            <input type="radio" name="inputOption" value="option1" autocomplete="off" checked>Text input
        </label>
        <label class="btn btn-success">
            <input type="radio" name="inputOption" value="option2" autocomplete="off">File upload
        </label>
    </div>
</div>

<%--INPUT FIELDS FOR SEQUENCES--%>

<%-- Text Input --%>
<div class="container mt-4 input-option" id="input-option1">
    <h3>Fasta input:</h3>
    <form action="${pageContext.request.contextPath}/translation" method="post" id="fasta-form">
        <label for="fasta-text">Fasta file:</label>
        <textarea rows="10" type="text" id="fasta-text" name="fasta" class="form-control" form="fasta-form"
                  required><c:if
                test="${requestScope.contents != null}">${requestScope.contents}</c:if></textarea>

        <jsp:include page="/includes/orfoptions.jsp"/>

        <br/>
        <c:if test="${requestScope.contents == null}">
            <button type="submit" value="Upload File" class="btn btn-success">Submit</button>
        </c:if>
        <c:if test="${requestScope.contents != null}">
            <button type="submit" value="Upload File" class="btn btn-success">Change data</button>
        </c:if>
    </form>
</div>

<%-- File input --%>
<div class="container mt-4 input-option d-none" id="input-option2">
    <h3>Not available yet</h3>
    <%--<form action="${pageContext.request.contextPath}/translation" method="post" enctype="multipart/form-data">--%>
    <%--<label for="fasta-file">Fasta file:</label>--%>
    <%--<input id="fasta-file" type="file" name="file" size="50" class="form-control" required/>--%>

    <%--<jsp:include page="/includes/orfoptions.jsp"/>--%>

    <%--<br/>--%>
    <%--<c:if test="${requestScope.contents == null}">--%>
    <%--<button type="submit" value="Upload File" class="btn btn-success">Submit</button>--%>
    <%--</c:if>--%>
    <%--<c:if test="${requestScope.contents != null}">--%>
    <%--<button type="submit" value="Upload File" class="btn btn-success">Change data</button>--%>
    <%--</c:if>--%>
    <%--</form>--%>
</div>


<jsp:include page="/includes/footer.jsp"/>
