<%--
  Created by IntelliJ IDEA.
  User: Sven
  Date: 02/12/2017
  Time: 10:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="/includes/header.jsp"/>

<%-- Sequence overview --%>
<div class="container mt-4" id="sequence">
    <h2>${requestScope.sequence.header}</h2>
    <c:set var="complement" value="${requestScope.sequence.complement}"/>
    <c:set var="sequence" value="${requestScope.sequence.sequence}"/>
    <c:set var="aa" value="${requestScope.sequence.translation}"/>
    <c:forEach var="sequence" items="${requestScope.sequence.sequence}" varStatus="status">
        <div id="sequence-div">
            <c:forEach var="i" begin="0" end="${fn:length(aa)}" step="1">
                <c:if test="${aa[i].type > 0}">
                    <p class="sequence">${aa[i].sequence[status.index]}</p>
                </c:if>
            </c:forEach>

            <p class="sequence">${sequence}</p>

            <p class="sequence"><c:forEach var="i" begin="1" end="${fn:length(sequence)}" step="1">|</c:forEach></p>

            <p class="sequence">${complement[status.index]}</p>

            <c:forEach var="i" begin="0" end="${fn:length(aa)}" step="1">
                <c:if test="${aa[i].type < 0}">
                    <p class="sequence reverse">${aa[i].sequence[status.index]}</p>
                </c:if>
            </c:forEach>
        </div>
        <hr/>
    </c:forEach>

</div>

<%-- History overview --%>
<div class="container mt-4" id="history">
    <h2>History</h2>
    <div id="history-wrapper">
        <c:forEach var="sequenceObject" items="${sessionScope.previous}" varStatus="status">
            <p>
                <a href="${pageContext.request.contextPath}/translation?history=${status.index}" class="history-link">
                    <c:choose>
                        <c:when test="${sequenceObject.header != null}">
                            ${sequenceObject.header}
                        </c:when>
                        <c:otherwise>
                            ${sequenceObject.sequence[0]}
                        </c:otherwise>
                    </c:choose>
                </a>
            </p>
            <hr/>
            <br/>
        </c:forEach>
    </div>
</div>


<jsp:include page="/includes/footer.jsp"/>