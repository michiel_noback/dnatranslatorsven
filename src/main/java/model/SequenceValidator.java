package model;

/**
 * Class with a static validation function for checking if a sequence is a valid DNA sequence
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
class SequenceValidator {

    /**
     * Checks if the sequence is a valid DNA sequence
     *
     * @param splicedContents The sequence in blocks of 80
     * @return boolean indicating if the sequence is valid
     */
    public static boolean validate(String[] splicedContents) {
        boolean valid = true;
        for (String line : splicedContents) {
            line = line.trim();
            if (!(line.startsWith(">")) && !(line.matches("[ACTG]+"))) {
                valid = false;
            }
        }
        return valid;
    }

}
