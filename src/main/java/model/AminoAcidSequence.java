package model;

import java.util.*;

/**
 * Class for an aminoacid dnaSequence, contains all relevant information for the dnaSequence
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class AminoAcidSequence {

    private int type;
    private int readFrame;
    private int nucleotideLength;
    private StringBuilder dnaSequence = new StringBuilder();
    private String[] finishedSequence;
    private int minORFLength;

    /**
     * Constructor for the object, initializes mandatory variables
     * @param readFrame Indicates which readFrame the dnaSequence is based on (e.g. 1, 2, 3, -1, -2, -3)
     * @param sequenceLength the length of the nucleotide dnaSequence
     * @param minORFLength the minimal length of an ORF
     */
    public AminoAcidSequence(int readFrame, int sequenceLength, int minORFLength) {
        this.readFrame = readFrame;
        this.minORFLength = minORFLength;
        this.nucleotideLength = sequenceLength;
        this.type = (int) Math.signum(readFrame);
        if (readFrameHasOffset()) {
            dnaSequence.append(getSpacesTail(Math.abs(readFrame) - 1));
        }
    }

    private String getSpacesTail(int numberOfSpaces) {
        return String.join("", Collections.nCopies(numberOfSpaces, " "));
    }

    private boolean readFrameHasOffset() {
        return Math.abs(readFrame) != 1;
    }

    /**
     * Add an amino acid to the dnaSequence based on a three letter codon
     * @param codon three letter string consisting of A,C,G and/or T
     */
    public void addCodon(String codon) {
        // Based on if the dnaSequence is on the forward or the reverse strand add the codon at a specific position
        if (type < 0) {
            dnaSequence.insert(0, Codons.getAminoAcid(codon)).insert(0, "  ");
        } else {
            dnaSequence.append(Codons.getAminoAcid(codon)).append("  ");
        }
    }

    /**
     * Generates the dnaSequence when the dnaSequence is on the reverse strand, this requires a bit more processing than
     * the forward strand to produce a correct format.
     */
    private void generateSequence() {
        // Extend the length to the length of the nucleotide dnaSequence
        if (dnaSequence.length() < nucleotideLength) {
            dnaSequence.insert(0, getSpacesTail((nucleotideLength) - dnaSequence.length()));
        }

        // How many characters are left after everything is splitted in blocks of 80?
        int leftover = dnaSequence.length() % 80;

        // Use the leftover index to get the last part
        String lastPart = dnaSequence.substring(dnaSequence.length() - leftover);
        // Get the rest of the dnaSequence
        String duplicate = dnaSequence.substring(0, dnaSequence.length() - leftover);

        // Split the rest of the dnaSequence in blocks of 80 and add the lastPart to the end
        List<String> splitted = new ArrayList<>(Arrays.asList(duplicate.split("(?<=\\G.{80})")));
        splitted.add(lastPart);


        // Reverse the dnaSequence so ORF marking is done from the end to the start (as it should be for the reverse strand)
        Collections.reverse(splitted);

        finishedSequence = findORF(splitted);
    }

    /**
     * Overloaded method of findORF for a String array instead of a String list
     * @param splitted contains all the dnaSequence blocks with a max length of 80
     * @return An array containing all the dnaSequence blocks with <mark></mark> elements for the ORF's
     */
    private String[] findORF(String[] splitted) {
        List<String> listSplitted = new ArrayList<>(Arrays.asList(splitted));
        return findORF(listSplitted);
    }

    /**
     * Marks the orf's in the dnaSequence, preserving the 80 width blocks, this needs to be done since there is no way
     * of knowing how many marks are in a block, so it can't be done afterwards
     * @param splitted contains all the dnaSequence blocks with a max length of 80
     * @return An array containing all the dnaSequence blocks with <mark></mark> elements for the ORF's
     */
    String[] findORF(List<String> splitted) {
        // Set variables for the operations
        boolean openORF = false;
        boolean openMark = false;
        String openMarker = "<mark>";
        String closeMarker = "</mark>";
        // Reverse to make them correct when reversed at the end of this method
        if (type < 0) {
            closeMarker = ">kram<";
            openMarker = ">kram/<";
        }

        HashMap<Integer, String> old = new HashMap<>();
        int orfLength = 0;

        for (int i = 0; i < splitted.size(); i++) {
            StringBuilder markedSequence = new StringBuilder();

            // Save the dnaSequence for reverting to the old one if necessary
            old.put(i, splitted.get(i));

            // If the previous block contained a still open ORF start with a open marker
            if (openORF) {
                markedSequence.append(openMarker);
                openMark = true;
            }
            for (int x = 0; x < splitted.get(i).length(); x++) {
                char c = splitted.get(i).toCharArray()[x];
                // If reverse strand, get the characters in the reversed order
                if (type < 0) {
                    c = splitted.get(i).toCharArray()[splitted.get(i).length() - x - 1];
                }
                // Open ORF
                if (c == 'M' && !openORF) {
                    markedSequence.append(openMarker);
                    markedSequence.append(c);
                    openMark = true;
                    openORF = true;
                    orfLength++;
                } else if ((c == '*' && openORF) || (i == splitted.size() - 1 && x == splitted.get(i).length() - 1)) {
                    // If stop sign AND currently open ORF end the ORF
                    orfLength++;
                    markedSequence.append(c);
                    markedSequence.append(closeMarker);
                    // If the orf length is shorter than the required length, revert to the old dnaSequence without the marks
                    // based on the hashmap

                    if (orfLength < minORFLength || orfLength <= 1) {
                        for (int key : old.keySet()) {
                            splitted.set(key, old.get(key));
                        }
                        if (type < 0) {
                            markedSequence = new StringBuilder(splitted.get(i).substring(splitted.get(i).length() - x - 1)).reverse();
                        } else {
                            markedSequence = new StringBuilder(splitted.get(i).substring(0, x + 1));
                        }
                    }
                    // Clear the revert hashmap
                    old.clear();
                    // Save the dnaSequence for reverting to the old one if necessary
                    old.put(i, splitted.get(i));

                    openMark = false;
                    openORF = false;
                    orfLength = 0;
                } else {
                    // If aminoacid and not a whitespace
                    if (openORF && Character.isLetter(c)) {
                        orfLength++;
                    }
                    markedSequence.append(c);
                }
            }
            // If the mark has not been closed, do this
            if (openMark) {
                markedSequence.append(closeMarker);
                openMark = false;
            }

            // If reverse strand, reverse the dnaSequence
            if (type < 0) {
                markedSequence.reverse();
            }

            splitted.set(i, markedSequence.toString());

        }

        // Reverse the whole collection to make everything correct again
        if (type < 0) {
            Collections.reverse(splitted);
        }
        return splitted.toArray(new String[0]);
    }

    /**
     * Returns the processed dnaSequence to the user
     * @return The processed and marked dnaSequence in block of 80 characters or less
     */
    public String[] getDnaSequence() {
        if (type < 0) {
            if (finishedSequence == null) {
                generateSequence();
            }
            return finishedSequence;
        } else {
            return findORF(dnaSequence.toString().split("(?<=\\G.{80})"));
        }
    }

    public void setDnaSequence(String dnaSequence) {
        this.dnaSequence = new StringBuilder(dnaSequence);
    }

    public int getType() {
        return type;
    }

    public int getReadFrame() {
        return readFrame;
    }
}
