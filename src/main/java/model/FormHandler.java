package model;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the POST data passed on from the servlet
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class FormHandler {

    public String contents;
    private Exception exp;
    private List<String> selectedOptions = new ArrayList<>();
    private Sequence sequence;

    /**
     * Controller for processing the form data
     *
     * @param isMultiForm does the form contain a file upload?
     * @param request     The request object containing all POST parameters
     * @param filePath    The path of the sequence file
     */
    public void processFormData(boolean isMultiForm, HttpServletRequest request, String filePath) {

        if (!isMultiForm) {
            sequence = new Sequence(request.getParameter("fasta"), request.getParameter("start"),
                    request.getParameter("stop"));
            sequence.translate(request.getParameterValues("options"), request.getParameter("orf-length"));
        } else {
            // CURRENTLY NOT USED, is for future development
            sequence = new Sequence(request.getParameter("start"), request.getParameter("stop"));

            uploadFile(request, filePath);

            sequence.setSequence(contents);
            sequence.translate(selectedOptions.toArray(new String[selectedOptions.size()]),
                    request.getParameter("orf-length"));
        }
    }

    /**
     * Handles the file as a upload but keeps it in memory instead of writing it to a file
     *
     * @param request  The request object containing all POST parameters
     * @param filePath The path of the sequence file
     */
    private void uploadFile(HttpServletRequest request, String filePath) {
        int maxSize = 10 * 1024 * 1024;
        int maxMem = 10 * 1024 * 1024;
        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(maxMem);

        factory.setRepository(new File(filePath));

        ServletFileUpload upload = new ServletFileUpload(factory);

        upload.setSizeMax(maxSize);

        try {

            List<FileItem> items = upload.parseRequest(request);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    selectedOptions.add(item.getString());
                } else {
                    contents = item.getString();
                }
            }

        } catch (FileUploadException exp) {
            this.exp = exp;
        }
    }

    public Sequence getSequence() {
        return sequence;
    }

}
