package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Class for a nucleotide sequence, contains all relevant information for the sequence
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class Sequence {

    private static final HashMap<Character, String> nucleotides = new HashMap<Character, String>() {

        {
            put('A', "T");
            put('C', "G");
            put('G', "C");
            put('T', "A");
        }
    };
    private String sequence;
    private String complement;
    private List<AminoAcidSequence> translation = new ArrayList<>();
    private String header;
    private boolean valid;
    private int start;
    private int stop;

    /**
     * Basic constructor for defining the start and stop location of the sequence
     *
     * @param start start location
     * @param stop  stop location, indicated from the END of the sequence
     */
    public Sequence(String start, String stop) {
        this.start = Integer.parseInt(start);
        this.stop = Integer.parseInt(stop);
    }

    /**
     * More advanced constructor which also sets the sequence
     * @param sequence The nucleotide sequence
     * @param start start location
     * @param stop stop location, indicated from the END of the sequence
     */
    public Sequence(String sequence, String start, String stop) {
        this(start, stop);
        setSequence(sequence);
    }

    /**
     * Determine the complement strand of the sequence
     */
    private void calculateComplement() {
        StringBuilder newComplement = new StringBuilder();
        for (char x : Arrays.toString(getSequence()).toCharArray()) {
            if (nucleotides.get(x) != null) {
                newComplement.append(nucleotides.get(x));
            }
        }
        this.complement = newComplement.toString();
    }

    /**
     * For every possible frame, translate the sequence (if that frame is requested by the user)
     * @param type An array which frames are requested by the user
     * @param minLength the minimal length of the ORF
     */
    public void translate(String[] type, String minLength) {
        if (valid) {
            if (Arrays.asList(type).contains("6-frame")) {
                for (int i = -3; i <= 3; i++) {
                    if (i != 0) {
                        translateFrame(i, Integer.parseInt(minLength));
                    }
                }
            } else {
                for (String index : type) {
                    translateFrame(Integer.parseInt(index), Integer.parseInt(minLength));
                }
            }
        }
    }

    /**
     * Translate the sequence to an amino acid sequence
     * @param frameIndex which frame is being translated (e.g. -3, -2, -1, 1, 2 or 3)
     * @param minLength the minimal length of an ORF to be displayed
     */
    private void translateFrame(int frameIndex, int minLength) {
        AminoAcidSequence aaSequence = new AminoAcidSequence(frameIndex, sequence.length(), minLength);
        String translateSequence;
        if (frameIndex > 0) {
            translateSequence = sequence;
        } else {
            translateSequence = new StringBuilder(complement).reverse().toString();
        }
        // Use the frame index to determine the start position in combination with the given start location
        // Stop at the sequence length minus the given stop location
        for (int i = Math.abs(frameIndex) - 1 + start; i < sequence.length() - stop; i += 3) {
            if (i + 2 < translateSequence.length()) {
                aaSequence.addCodon(translateSequence.substring(i, i + 3));
            }
        }
        translation.add(aaSequence);
    }

    /**
     * Sets the given sequence for the object
     *
     * @param sequence the nucleotide sequence
     */
    public void setSequence(String[] sequence) {
        StringBuilder newSequence = new StringBuilder();
        for (String line : sequence) {
            line = line.trim();
            if (line.startsWith(">")) {
                header = line;
            } else if (line.matches("[ACTG]+")) {
                newSequence.append(line.trim());
            }
        }
        this.sequence = newSequence.toString();
        calculateComplement();
    }

    /**
     * Sets the given sequence for the object if the sequence is valid
     * @param sequence the nucleotide sequence
     */
    public void setSequence(String sequence) {
        String[] splicedContents = sequence.trim().split("\n");
        valid = SequenceValidator.validate(splicedContents);
        if (valid) {
            setSequence(splicedContents);
        }
    }

    public String[] getComplement() {
        return complement.split("(?<=\\G.{80})");
    }

    public String[] getSequence() {
        return sequence.split("(?<=\\G.{80})");
    }

    public void setSequence(FormHandler upload) {
        setSequence(upload.contents);
    }

    public List<AminoAcidSequence> getTranslation() {
        return translation;
    }

    public String getHeader() {
        return header;
    }

    public boolean isValid() {
        return valid;
    }
}
