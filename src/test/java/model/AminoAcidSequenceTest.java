package model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AminoAcidSequenceTest {

    @Test
    public void findORFReverse() {
        String[] inputArray = {"G  A  *  A  P  G  R ", "  P  V  C  R  A  A  S  S  M  E  T  P  P  S  A  R  L  G  P  G  A  C  G  G  S  G  ",
        " S  A  *  P  R  C  C  C  *  R  T  S  R  A  A  G  P  V  A  S  V  W  S  E  A  C  A",
        "Q  A  L  W  D  A  P  R  G  R  G  P  R  V  V  *  P  S  A  D  P  W  A  P  R  A  T ",
        "  P  G  P  S  G  V  A  S  V  V  C  G  A  P  A  S  R  S  Q  G  P  R  S  P  L  W  ",
        "    T  R  S  G  R  G  P  A  R  A  L  R  A  P  G  A  S  R  C  L  S  A  A  P  R  D"};
        String[] expectedOutput = {"    T  R  S  G  R  G  P  A  R  A  L  R  A  P  G  A  S  R  C  L  S  A  A  P  R  D",
        "  P  G  P  S  G  V  A  S  V  V  C  G  A  P  A  S  R  S  Q  G  P  R  S  P  L  W  ",
        "Q  A  L  W  D  A  P  R  G  R  G  P  R  V  V  *  P  S  A  D  P  W  A  P  R  A  T ",
        " S  A  *  P  R  C  C  C  *  R  T  S  R  A  A  G  P  V  A  S  V  W  S  E  A  C  A",
        "<mark>  P  V  C  R  A  A  S  S  M</mark>  E  T  P  P  S  A  R  L  G  P  G  A  C  G  G  S  G  ",
        "G  A  *  A  P  G  R "};

        AminoAcidSequence aa = new AminoAcidSequence(-2, 350, 10);

        assertArrayEquals("The logic has been corrupted! The output isn't wat it should be", expectedOutput, aa.findORF(Arrays.asList(inputArray)));
    }

    @Test
    public void findORFForward() {
        String[] inputSeq = {" S  H  R  A  K  L  D  E  L  L  K  L  K  A  G  S  S  I  T  W  L  T  W  L  R  Q  A",
        "  P  L  K  P  N  S  R  H  M  L  E  H  I  E  R  L  K  T  F  Q  L  V  D  L  P  E  ",
        "G  L  G  R  H  I  H  Q  N  R  L  L  K  L  A  R  E  G  G  Q  M  T  P  K  D  L  G ",
        " K  F  E  P  Q  R  R  Y  A  T  L  A  A  V  V  L  E  S  T  A  T  V  I  D  E  L  V",
        "  D  L  H  D  R  I  L  V  K  "};
        String[] expectedOutput = {" S  H  R  A  K  L  D  E  L  L  K  L  K  A  G  S  S  I  T  W  L  T  W  L  R  Q  A",
        "  P  L  K  P  N  S  R  H  <mark>M  L  E  H  I  E  R  L  K  T  F  Q  L  V  D  L  P  E  </mark>",
        "<mark>G  L  G  R  H  I  H  Q  N  R  L  L  K  L  A  R  E  G  G  Q  M  T  P  K  D  L  G </mark>",
        "<mark> K  F  E  P  Q  R  R  Y  A  T  L  A  A  V  V  L  E  S  T  A  T  V  I  D  E  L  V</mark>",
        "<mark>  D  L  H  D  R  I  L  V  K  </mark>"};

        AminoAcidSequence aa = new AminoAcidSequence(2, 354, 10);

        assertArrayEquals("The logic has been corrupted! The output isn't wat it should be", expectedOutput, aa.findORF(Arrays.asList(inputSeq)));
    }

}